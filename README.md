Exercício 2

Crie seu próprio repositório no GitLab denominado AlunoTP.

Crie uma pasta chamada projeto e exercicios e na pasta exercícios inclua a resolução dos
exercícios Git.

Envie o commit para o repositório remoto.

Adicione um arquivo denominado README.md ao projeto e inclua o seguinte conteúdo
Repositório de exercícios das aulas de Técnicas de Programação

Adicione uma nova pasta dentro da pasta exercícios chamado exercicio_java. Busque um
exercício que você já realizou e suba para o repositório.

Crie também um arquivo chamado README.md na pasta raiz da pasta e inclua o cabeçalho deste
exercício.